<?php

require_once 'controllers.php';
$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

if ('/login.php' == $uri) {
  get_list();
}

elseif ('/index1.php/list' == $uri) {
  show_all();
}

elseif (('/index1.php/show') == $uri && isset($_GET['link'])) {
  $id = $_GET['link'];
  get_one($id);
}

elseif ('/index1.php/comment' == $uri) {
  put_comment();
}

elseif ('/index1.php/create' == $uri) {

  require_once 'article.html';
}

elseif ('/index1.php/post' == $uri) {

  post_article();
}

elseif ('/index1.php/skip' == $uri) {
  viewarticle();
}

elseif (('/index1.php/edit' == $uri) && isset($_GET['link'])) {
  $id = $_GET['link'];
  edit($id);
}

elseif ('/index1.php/update' == $uri) {
  update();
}

elseif (('/index1.php/index1.php/vieweach' == $uri) && isset($_GET['link'])) {
  $id = $_GET['link'];
  vieweach($id);
}

else {
  echo $uri;
  header('http/1.1 404 Not Found');
  echo "<h1>Page Not Found</h1>";
}
?>