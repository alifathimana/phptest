<?php

/**
 * Gets the id and title of all the articles. */
function get_list() {
  $link = open_database_connection();
  $articles = get_arts($link);
  close_database_connection($link);
  require 'templates/list.tpl.php';
}

/**
 * Returns the details of the selected article.
 */
function get_one($id) {
  require_once 'model.php';
  $link = open_database_connection();
  $article = get_art($id, $link);
  $comment = get_comments($id, $link);
  close_database_connection($link);
  require 'templates/show.tpl.php';
}

/**
 * Posts comments.
 */
function put_comment() {
  require_once 'model.php';
  $link = open_database_connection();
  comment($link);
}

/**
 * Creates a new article and saves it to database.
 */
function post_article() {
  require_once 'model.php';
  $link = open_database_connection();
  post($link);
}

/**
 * Function to prepopulate the text fields to facilitate editing.
 */
function edit($id) {
  require_once 'model.php';
  $link = open_database_connection();
  $result = editart($id, $link);
  require 'templates/article.tpl.php';

}

/**
 * Stores the updated value to database.
 */
function update() {
  require_once 'model.php';
  $link = open_database_connection();
  updateart($link);
}

/**
 * Shows all.
 */
function show_all() {

  require_once 'model.php';
  $link = open_database_connection();
  $articles = show($link);
  require 'templates/list.tpl.php';
}

/**
 * View article.
 */
function viewarticle() {

  require_once 'model.php';
  $link = open_database_connection();
  $articles = view($link);
  require 'templates/view.tpl.php';
}

/**
 * Function to show each article.
 */
function vieweach($id) {
  require_once 'model.php';
  $link = open_database_connection();
  $showart = view_each($id, $link);
  require 'templates/showeach.tpl.php';
}
?>
