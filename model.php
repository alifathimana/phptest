<?php

/**
 * Open connection.
 */
function open_database_connection() {
  $servername = "localhost";
  $dbname = "test";
  $username = "root";
  $password = "root";
  $link = new PDO("mysql:servername=$servername;dbname=$dbname", $username, $password);
  $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $link;
}

/**
 * Close connection.
 */
function close_database_connection($link) {
  $link = NULL;
}

/**
 * Gets all articles.
 */
function get_arts($link) {
  $stmt = $link->prepare("SELECT * FROM articles");
  $stmt->execute();
  $arts = $stmt->fetchAll();
  return $arts;
}

/**
 * Get each article.
 */
function get_art($id, $link) {
  $stmt = $link->prepare("SELECT * FROM articles WHERE id = :id");
  $stmt->bindParam(':id', $id);
  $stmt->execute();
  $art = $stmt->fetchAll();
  return $art;
}

/**
 * Displays comments.
 */
function get_comments($id, $link) {
  $stmt = $link->prepare("SELECT * FROM comments WHERE number = :num ORDER BY id ASC");
  $stmt->bindParam(':num', $id);
  $stmt->execute();
  $comm = $stmt->fetchAll();
  return $comm;
}

/**
 * To post comment.
 */
function comment($link) {
  $a = $_POST['value1'];
  $b = $_POST['value2'];
  $c = $a + $b;
  $name = $_POST['Name'];
  $comment = $_POST['Message'];
  $date = date("M d");
  $time = date("g:i A");;
  $no = $_POST['number'];
  $cap = $_POST['captcha'];
  if ($c == $cap) {
    $stmt = $link->prepare("INSERT INTO comments (number, name, date, time, comment) VALUES (:number, :name, :date, :time, :comment)");
    $stmt->bindParam(':number', $no);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':time', $time);
    $stmt->bindParam(':comment', $comment);
    $stmt->execute();
    echo "Comment added successfully!<br>";
    echo "<a href = '/index1.php/show?link=$no'>Back</a>";
  }

  else {
    echo "Incorrect captcha";
    echo "<a href = '/index1.php/show?link=$no'>Back</a>";
  }
  $_POST = array();
}

/**
 * Adds a new article.
 */
function post($link) {
  $t1 = $_POST['title'];
  $d1 = date("Y-m-d");
  $a1 = $_POST['name'];
  $b1 = $_POST['body'];
  $stmt = $link->prepare("INSERT INTO articles (title, date, author, body) values (:title, :date, :author, :body)");
  $stmt->bindParam(':title', $t1);
  $stmt->bindParam(':date', $d1);
  $stmt->bindParam(':author', $a1);
  $stmt->bindParam(':body', $b1);
  $stmt->execute();
  echo "Article added successfully!";
  //echo "<a href = '/login.php'>Back to articles</a>";
  $_POST = array();
}

/**
 * Prepopulates article edit fields.
 */
function editart($id, $link) {
  $stmt = $link->prepare("SELECT * FROM articles WHERE id = :id");
  $stmt->bindParam(':id', $id);
  $stmt->execute();
  $res = $stmt->fetchAll();
  return $res;
}

/**
 * Updates article.
 */
function updateart($link) {
  $i = $_POST['id'];
  $t = $_POST['title'];
  $n = $_POST['name'];
  $b = $_POST['body'];
  $d = date("Y-m-d");
  $stmt = $link->prepare("UPDATE articles SET title = :title, date = :date, author = :author, body = :body WHERE id = :id ");
  $stmt->bindParam(':title', $t);
  $stmt->bindParam(':date', $d);
  $stmt->bindParam(':author', $n);
  $stmt->bindParam(':body', $b);
  $stmt->bindParam(':id', $i);
  $stmt->execute();
  echo "Article edited successfully!<br><br><br><br>" ;
  echo "<a href = '/index1.php/show?link=$i'>Back to article</a>" ;
}

/**
 * Displays article.
 */
function show($link) {
  $stmt = $link->prepare("SELECT * FROM articles");
  $stmt->execute();
  $allarts = $stmt->fetchAll();
  return $allarts;
}

/**
 * View article.
 */
function view($link) {

  $stmt = $link->prepare("SELECT * FROM articles");
  $stmt->execute();
  $all = $stmt->fetchAll();
  return $all;
}

/**
 * Function to view articles.
 */
function view_each($id, $link) {

  $stmt = $link->prepare("SELECT * FROM articles WHERE id = :id");
  $stmt->bindParam(':id', $id);
  $stmt->execute();
  $art = $stmt->fetchAll();
  return $art;
}
?>